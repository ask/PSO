clear all;

%% Paramters
generations = 100;
N = 200; %population
dims = 3;
%sigma = 4; %neighboorhood must be smaller than N
phi1_max = 2.5; %cognition learning ratge from book
phi2_max = 1.3; %social learning rate
phi3_max = 1.3; %historical learning rate
w = .4; %Weight
v_max = 1; %between 10 and 20% of the search space range

% Search space max min
maxmin = [ 0  30 % PEA
          -30  0 % AEA
           0  2];% G_mh
%

%% init
v = rand(N, dims) - .5; %velocity
%position = zeros(N, dims);
position = [(rand(N,1)*(maxmin(1,2)-maxmin(1,1))+maxmin(1,1)) ...
            (rand(N,1)*(maxmin(2,2)-maxmin(2,1))+maxmin(2,1)) ...
            (rand(N,1)*(maxmin(3,2)-maxmin(3,1))+maxmin(3,1))];
best_position = position; %best position;

inds = zeros(N, 3, dims);
inds(:, 1, :) = position;
inds(:, 2, :) = best_position;
inds(:, 3, :) = v;

performance = zeros(N,1); % scores for every

best = 0;
bscore = 0;
%bestpositer : best position from last iteration
%bestpos : best historical postion

%file = 'workspaces/laptop';
file = 'workspaces/tower';
%file = '/u/halle/skripnik/matlab_shared/';

best_score_per_particle = zeros(N, 1);

history = zeros(generations, N, 3);
bestposhistory = zeros(generations, 3);
scorehistory = zeros(generations, 1);
tic
for generation = 1:generations
    for ind = 1:N
       [v, d] = simFox(reshape(inds(ind, 1, :), 1, 3));
       performance(ind) = v;%sum(vd);
    end
    [bscoreiter, bestiter] = max(performance);
    bestpositer = inds(bestiter, 1, :);
    scorehistory(generation) = bscoreiter;
    bestposhistory(generation, :) = reshape(bestpositer, 1, 3);
    if bscoreiter > bscore
        %[bscore, best] = max(performance);
        bscore = bscoreiter;
        bestpos = bestpositer;
    end
    for ind = 1:N
        % set phi
        phi1 = rand * phi1_max;
        phi2 = rand * phi2_max;
        phi3 = rand * phi3_max;
        
        %set best score
        if performance(ind) > best_score_per_particle(ind)
            inds(ind, 2, :) = inds(ind, 1, :);
            best_score_per_particle(ind) = performance(ind);
        end
        
        % calculate velocity
        inds(ind, 3, :) = inds(ind, 3, :) * w + ...
            phi1 * (inds(ind, 2, :) - inds(ind, 1, :)) + ...
            phi2 * ((bestpositer) - inds(ind, 1, :)) + ...
            phi3 * (bestpos - inds(ind, 1,: ));
        
        % enforce max velocity
        for i = 1:dims
            if abs(inds(ind, 3, i)) > ((maxmin(i, 2) - maxmin(i, 1)) * .1)
                inds(ind, 3, i) = sign(inds(ind, 3, i)) * ((maxmin(i, 2) - maxmin(i, 1)) * .1);
            end
        end
        %if norm(reshape(inds(ind, 3, :), 3, 1)) > v_max
        %    inds(ind, 3, :) = inds(ind, 3, :) * v_max / norm(reshape(inds(ind, 3, :), 3, 1));
        %end
        %apply velocity
        inds(ind, 1, :) = inds(ind, 1, :) + inds(ind, 3, :);
        %enforce minmax
        for i = 1:dims
            if inds(ind, 1, i) < maxmin(i, 1)
                inds(ind, 1, i) = maxmin(i, 1);
            elseif inds(ind, 1, i) > maxmin(i, 2)
                inds(ind, 1, i) = maxmin(i, 2);
            end
        end
    end
    disp(["generation: ", generation, ", bscore: ", bscore, "phi1", phi1, "phi2", phi2, "vel:", ...
        reshape(inds(ind, 3, :), 1, 3), reshape((inds(ind, 2, :) - inds(ind, 1,: )), 1, 3), reshape((bestpos - inds(ind, 1,: )), 1, 3)])
    history(generation, :, :) = reshape(inds(:, 1, :), N, 3);
end
time = toc;
save(strcat(file, date, "_generations:", num2str(generations), "_population:", num2str(N), "phi123max:", ...
    num2str(phi1_max), "_", num2str(phi2_max), "_", num2str(phi3_max), "_solo.mat"))
%disp(inds(best,1,:))