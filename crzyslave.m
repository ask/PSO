file = '/u/halle/skripnik/matlab_shared/';

while true
    try
        load(strcat(file, 'performance'), 'performance')
    catch
        disp("could not read file. I try again.")
    end
    [score, position] = min(performance); 
    if score == -2
        disp(strcat("got work package. Working on position: ", num2str(position)))
        performance(position) = -1;
        save(strcat(file, 'performance'), 'performance');
        load(strcat(file, 'queue'), 'queue');
        [v, d] = simFox(queue(position, :));
        performance(position) = v;
        save(strcat(file, 'performance'), 'performance');
        disp("done")
    else 
        disp("waiting for work")
        pause(1); %wait 1 sec
    end
    
end