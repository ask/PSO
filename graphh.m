%figure
%hold on
clf
subplot(1,3,1)
xlabel({'PEA'});
ylabel({'AEA'});
zlabel({'G_{mh}'});
ax.GridAlpha = 1;
view(-45, 20);
grid on
% Create title
title({'Evolution of particle swarm'});
hold on
colormap jet
subplot(1,3,2)
xlabel({'PEA'});
ylabel({'AEA'});
zlabel({'G_{mh}'});

view(-45, 20);
grid on
% Create title
title({'Best position path'});
hold on

subplot(1,3,3)
xlabel({'generation'});
ylabel({'performance'});

grid on
% Create title
title({'Best iteration performance'});
hold on



for i=1:generations
    subplot(1,3,1)
    head1 = scatter3(history(i,:,1), history(i, :, 2), history(i, :, 3), 'b', 'filled');
    set(gca,'XLim',maxmin(1, :),'YLim',maxmin(2, :),'ZLim',maxmin(3, :))
    title({strcat('Evolution of particle swarm. Generation:', num2str(i))});
    drawnow
    subplot(1,3,2)
    if i>1
        head2 = plot3(bestposhistory(i-1:i,1), bestposhistory(i-1:i,2), bestposhistory(i-1:i,3), 'b');
    end
    set(gca,'XLim',maxmin(1, :),'YLim',maxmin(2, :),'ZLim',maxmin(3, :))
    drawnow
    subplot(1,3,3)
    if i>1
        head3 = plot(i-1:i, scorehistory(i-1:i), "b--o");
    else
        head3 = plot(i, scorehistory(i), "b--o");
    end
    set(gca,'XLim',[0 generations],'YLim',[0 1]);
    drawnow
    pause(0.1)
    delete(head1)
    %delete(head2)
end